/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Ctrl.c
 *    \author     Pintea Radu-Nicolae
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Ctrl.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
   Steering_Parallel,
   Steering_Ackerman,
   Steering_Horn_Parallel,
   Steering_Horn_Ackerman,
   Configuration,
} CTRL_State_Type;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
Rte_JoyStsType CTRL_Joy_Final_Value;
Rte_ServoCtrlType CTRL_LEFT;
Rte_ServoCtrlType CTRL_RIGHT;
Rte_BuzzCtrlType CTRL_Buzz;
Rte_BuzzPeriodType CTRL_bu, longpress;
Rte_JoyPressType Falling_Edge, Last_Falling_Edge;
uint8 Falling_Edge_Detected;
CTRL_State_Type State, Last_State;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
void FallingEdge()
{
   Falling_Edge = CTRL_Joy_Final_Value.t_Press;
   // Last_Falling_Edge = CTRL_Joy_Final_Value.t_Press;
   if ((Last_Falling_Edge == RTE_JOY_PRESSED) && (Falling_Edge == RTE_JOY_NOT_PRESSED))
   {
      Falling_Edge_Detected = 1;

   }

   else
   {
      Falling_Edge_Detected = 0;

   }
   Last_Falling_Edge = Falling_Edge;

}
void Parallel()
{
   CTRL_LEFT = 3000 + ((15000 - 3000) * CTRL_Joy_Final_Value.t_AxisX) / 10000;
   CTRL_RIGHT = 3000 + ((15000 - 3000) * CTRL_Joy_Final_Value.t_AxisX) / 10000;

   Rte_Write_OutServo_ServoSteerLeft(&CTRL_LEFT);
   Rte_Write_OutServo_ServoSteerRight(&CTRL_RIGHT);
}
void Ackerman()
{
   CTRL_LEFT = 3000 + ((15000 - 3000) * CTRL_Joy_Final_Value.t_AxisX) / 10000;
   CTRL_RIGHT = 3000 + ((15000 - 3000) * CTRL_Joy_Final_Value.t_AxisX) / 10000;
   if (CTRL_RIGHT >= 9000 && CTRL_RIGHT <= 15000)
   {
      if (CTRL_RIGHT <= 9500)
      {
         CTRL_LEFT = ((((9654 / 1000) * CTRL_RIGHT) / 10) + (19 / 10)) / 100;
      }
      else if (CTRL_RIGHT > 9500 && CTRL_RIGHT <= 11500)
      {
         CTRL_LEFT = ((((8212 / 1000) * CTRL_RIGHT) / 10) + (836 / 100)) / 10;
      }

      else if (CTRL_RIGHT > 11500 && CTRL_RIGHT <= 13500)
      {
         CTRL_LEFT = ((((658 / 100) * CTRL_RIGHT) / 10) + (43264 / 1000)) / 10;
      }
      else if (CTRL_RIGHT > 13500)
      {
         CTRL_LEFT = ((((6116 / 1000) * CTRL_RIGHT) / 10) + (61664 / 1000)) / 10;
      }
   }
   else if (CTRL_LEFT >= 3000 && CTRL_RIGHT < 9000)
   {
      if (CTRL_LEFT <= 9000 && CTRL_LEFT >= 8500)
      {
         CTRL_RIGHT = ((((9654 / 1000) * CTRL_LEFT) / 10) + (19 / 10)) / 100;
      }
      else if (CTRL_LEFT < 8500 && CTRL_LEFT >= 6500)
      {
         CTRL_RIGHT = ((((8212 / 1000) * CTRL_LEFT) / 10) + (836 / 100)) / 10;
      }
      else if (CTRL_LEFT < 6500 && CTRL_LEFT >= 4500)
      {
         CTRL_RIGHT = ((((658 / 100) * CTRL_LEFT) / 10) + (43264 / 1000)) / 10;
      }
      else if (CTRL_LEFT >= 3000)
      {
         CTRL_RIGHT = ((((6116 / 1000) * CTRL_LEFT) / 10) + (61664 / 1000)) / 10;
      }
   }
   Rte_Write_OutServo_ServoSteerLeft(&CTRL_LEFT);
   Rte_Write_OutServo_ServoSteerRight(&CTRL_RIGHT);
}
void Buzz_Horn_ON()
{

   CTRL_Buzz.t_Period = 100 + ((500 - 100) * CTRL_Joy_Final_Value.t_AxisY) / 10000;
   CTRL_Buzz.t_Enable = RTE_BUZZ_ON;
   Rte_Write_OutBuzz_BuzzHorn(&CTRL_Buzz);

}
void Buzz_Horn_OFF()
{
   CTRL_Buzz.t_Enable = RTE_BUZZ_OFF;
   Rte_Write_OutBuzz_BuzzHorn(&CTRL_Buzz);
}
void Verificare()
{

   if ((Falling_Edge_Detected == 1) && (State == Steering_Parallel))
   {
      State = Steering_Horn_Parallel;
      Falling_Edge_Detected = 0;
   }

   if ((Falling_Edge_Detected == 1) && (State == Steering_Horn_Parallel))
   {
      State = Steering_Parallel;
      Falling_Edge_Detected = 0;
   }

   if ((Falling_Edge_Detected == 1) && (State == Steering_Ackerman))
   {
      State = Steering_Horn_Ackerman;
      Falling_Edge_Detected = 0;
   }
   if ((Falling_Edge_Detected == 1) && (State == Steering_Horn_Ackerman))
   {
      State = Steering_Ackerman;
      Falling_Edge_Detected = 0;
   }

}


/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Ctrl_Init()
{
   State = Steering_Parallel;
}

void Ctrl_Main()
{
   Rte_Read_InJoy_JoySteer(&CTRL_Joy_Final_Value);
   Verificare();
   FallingEdge();
   Verificare();

   switch (State)
   {
      case Steering_Parallel:
      {

         Parallel();
         Buzz_Horn_OFF();

         break;
      }
      case Steering_Ackerman:
      {

         Ackerman();
         Buzz_Horn_OFF();

         break;
      }
      case Steering_Horn_Parallel:
      {

         Parallel();
         Buzz_Horn_ON();

         break;
      }
      case Steering_Horn_Ackerman:
      {

         Ackerman();
         Buzz_Horn_ON();

         break;
      }
      case Configuration:
      {
         Buzz_Horn_OFF();
         CTRL_RIGHT = 18000;
         CTRL_LEFT = 0;
         break;
      }
   }

}

