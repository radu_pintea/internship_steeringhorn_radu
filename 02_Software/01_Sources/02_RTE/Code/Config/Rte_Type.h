/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Type.h
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Exports only the software component runnables (global functions), usually only one initialization
 *                and one cyclic runnable. This header must exist for every software component and its name shall be
 *                the short name of the component that is used as a prefix for all the global functions and variables
 *                inside the module.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef RTE_TYPE_H
#define RTE_TYPE_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"


/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

typedef uint16 Rte_JoyAxisType;
typedef uint16 Rte_ServoCtrlType;
typedef uint16 Rte_BuzzPeriodType;

typedef enum
{
   RTE_JOY_NOT_PRESSED,
   RTE_JOY_PRESSED,
} Rte_JoyPressType;

typedef enum
{
   RTE_BUZZ_OFF,
   RTE_BUZZ_ON,
} Rte_BuzzEnableType;

typedef struct
{
   Rte_JoyAxisType t_AxisX;
   Rte_JoyAxisType t_AxisY;
   Rte_JoyPressType t_Press;
} Rte_JoyStsType;

typedef struct
{
   Rte_BuzzEnableType t_Enable;
   Rte_BuzzPeriodType t_Period;
} Rte_BuzzCtrlType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* RTE_TYPE_H */
