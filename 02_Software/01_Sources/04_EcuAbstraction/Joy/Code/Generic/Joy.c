/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Joy.c
 *    \author     Pintea Radu-Nicolae
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Joy.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
Adc_ValueGroupType Array_X[3];
Adc_ValueGroupType Array_Y[3];
Rte_JoyPressType Joy_Btn;
Rte_JoyStsType Joy_Final_Value;
uint8 counter;
uint8 i;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Joy_Init()
{
   Joy_Btn = RTE_JOY_NOT_PRESSED;
}
void Joy_Main()
{
   Dio_LevelType Lvl;
   Rte_JoyPressType press;

   Adc_ValueGroupType Joy_AxisX;
   Adc_ValueGroupType Joy_AxisY;

   uint32 sum;
   uint8 it;
   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_X, &Joy_AxisX);
   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_X);
   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_X))
   {
      /* Wait for the conversion to finish. */
   }


   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_Y, &Joy_AxisY);
   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_Y);
   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_Y))
   {
      /* Wait for the conversion to finish. */
   }

   /*Moving Average for X Axis*/
   Array_X[i] = Joy_AxisX;

   if (i == 2)
   {
      i = 0;
   }
   else
   {
      i++;
   }
   sum = 0;
   for (it = 0; it < 3; it++)
   {
      sum += Array_X[it];
   }

   Joy_Final_Value.t_AxisX = sum / 3;
   Joy_Final_Value.t_AxisX = (Joy_Final_Value.t_AxisX * 10000) / 4095;

   /*Moving Average for Y Axis*/
   Array_Y[i] = Joy_AxisY;

   if (i == 2)
   {
      i = 0;
   }
   else
   {
      i++;
   }
   sum = 0;
   for (it = 0; it < 3; it++)
   {
      sum += Array_Y[it];
   }

   Joy_Final_Value.t_AxisY = sum / 3;
   Joy_Final_Value.t_AxisY = (Joy_Final_Value.t_AxisY * 10000) / 4095;

   /* Algoritmul de Debounce*/
   Lvl = Dio_ReadChannel(IOHWAB_JOY_STEER_BTN);
   if (Lvl == STD_LOW)
   {
      press = RTE_JOY_PRESSED;
   }
   else
   {
      press = RTE_JOY_NOT_PRESSED;
   }
   if (press != Joy_Btn)
   {
      counter++;
      if (counter >= 3)
      {
         Joy_Btn = press;
         counter = 0;
      }
      else
      {
      }

   }
   else
   {
      counter = 0;
   }

   Joy_Final_Value.t_Press = Joy_Btn;

   Rte_Write_Out_JoySteer(&Joy_Final_Value);
}
