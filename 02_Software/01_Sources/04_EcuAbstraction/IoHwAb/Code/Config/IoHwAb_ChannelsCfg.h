/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       IoHwAb_ChannelsCfg.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Defines the sensor and actuator channels on the ECU abstraction level for accessing all the ECU
 *                signals.
 *
 *    From the MCAL drivers' perspective the channels do not contain the information about the context they're used in.
 *    At this level the MCAL channels are mapped to ECU signal names that describe the origin, a sensor, or the
 *    destination, an actuator.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef IOHWAB_CHANNELSCFG_H
#define IOHWAB_CHANNELSCFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "IoHwAb_ApiCfg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Redefine all the MCAL driver channels by providing the information about the used peripherals. */

/* -------------------------- Analog ---------------------------*/

#if (STD_ON == IOHWAB_ADC_API)

#define IOHWAB_JOY_STEER_AXIS_X                 (ADC_C_4)
#define IOHWAB_JOY_STEER_AXIS_Y                 (ADC_C_5)

#endif /* (STD_ON == IOHWAB_ANALOG_API) */

/* -------------------------- Digital --------------------------*/

#if (STD_ON == IOHWAB_DIO_API)

#define IOHWAB_JOY_STEER_BTN                    (DIO_A_1)
#define IOHWAB_BUZZ_HORN_VCC                    (DIO_B_1)
#define IOHWAB_BUZZ_HORN_CTRL                   (DIO_B_0)

#endif /* (STD_ON == IOHWAB_DIGITAL_API) */

/* ---------------------------- PWM ----------------------------*/

#if (STD_ON == IOHWAB_PWM_API)

#define IOHWAB_SERVO_STEER_LEFT                 (PWM_D_14)
#define IOHWAB_SERVO_STEER_RIGHT                (PWM_D_15)

#endif /* (STD_ON == IOHWAB_PWM_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* IOHWAB_CHANNELSCFG_H */
