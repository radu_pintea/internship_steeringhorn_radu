/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Declares the AUTOSAR DIO standard and implementation data types, exports the global link-time
 *                configurations and exports the interfaces for controlling all the individual channels, channel groups
 *                and port instances.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIO_H
#define DIO_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Dio_Cfg.h"

#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Invalid configuration checks. */
#if (0U == DIO_NUMBER_OF_PORTS)
#error "Invalid DIO driver configuration. DIO_NUMBER_OF_PORTS shall be greater than 0."
#endif

#if ((STD_OFF == DIO_PORTS_API) && (STD_OFF == DIO_CHANNELS_API) && (STD_OFF == DIO_CHANNEL_GROUPS_API))
#error "Invalid DIO driver configuration. At least one API (DIO_PORTS_API, DIO_CHANNELS_API or \
DIO_CHANNEL_GROUPS_API) shall be activated."
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------- AUTOSAR Types --------------------------------------------------*/

/** \brief  Represents the type that holds the numeric IDs of all ports. */
typedef uint8 Dio_PortType;

#if (STD_ON == DIO_CHANNELS_API)
/** \brief  Represents all possible levels of a DIO channel (STD_HIGH and STD_LOW). */
typedef uint8 Dio_LevelType;

/** \brief  Represents the type that holds the numeric IDs of all DIO channels. */
typedef uint8 Dio_ChannelType;
#endif

#if ((STD_ON == DIO_CHANNEL_GROUPS_API) || (STD_ON == DIO_PORTS_API))
/** \brief  Represents the value of all the pins of a specified port. The GPIO data registers are 32-bit long but only
 *          the 16 least significant bits are used. */
typedef uint16 Dio_PortLevelType;
#endif

#if (STD_ON == DIO_CHANNEL_GROUPS_API)
/** \brief  Represents the definition of a channel group, consisting of several adjoining channels within a port. */
typedef struct
{
   /** \brief  Represents the positions of the channel group pins inside the port. */
   uint16 Mask;
   /** \brief  Represents the offset of the channel group in the port in number of bits, starting from LSB. */
   uint8 Offset;
   /** \brief  Represents the numeric ID of the port on which the channel group is configured. */
   Dio_PortType Port;
} Dio_ChannelGroupType;
#endif

/*----------------------------------------------- Implementation Types ----------------------------------------------*/

#if (STD_ON == DIO_CHANNELS_API)
/** \brief  Defines the DIO configuration type for one channel or port consisting of the port register on which a read
 *          or write operation takes place and the select mask that is needed for the operation. */
typedef struct
{
   /** \brief  Represents the select mask to be used for a write or read operation on the specified GPIO port. */
   uint16 us_SelectMask;
   /** \brief  Represents the port ID that contains the DIO channel. */
   Dio_PortType t_PortId;
} Dio_ChannelCfgType;
#endif

/** \brief  Represents the definition of a port, consisting of the base address of all the GPIO registers that are
 *          used for read / write operations. */
typedef struct
{
   GPIO_TypeDef * t_Registers;
} Dio_PortCfgType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == DIO_CHANNELS_API)
extern const Dio_ChannelCfgType Dio_gkat_Channels[DIO_NUMBER_OF_CHANNELS];
#endif

#if (STD_ON == DIO_CHANNEL_GROUPS_API)
extern const Dio_ChannelGroupType Dio_gkat_ChannelGroups[DIO_NUMBER_OF_CHANNEL_GROUPS];
#endif

extern const Dio_PortCfgType Dio_gkat_PortAdresses[DIO_NUMBER_OF_PORTS];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == DIO_CHANNELS_API)
extern Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId);
extern void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level);
extern Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId);
#endif

#if (STD_ON == DIO_CHANNEL_GROUPS_API)
extern Dio_PortLevelType Dio_ReadChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr);
extern void Dio_WriteChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr, Dio_PortLevelType Level);
#endif

#if (STD_ON == DIO_PORTS_API)
extern Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId);
extern void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level);
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* DIO_H */
