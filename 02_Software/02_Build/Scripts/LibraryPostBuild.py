import os

path = os.path.dirname(os.path.abspath(__file__))
path = path + "\\..\\..\\01_Sources"

print("Removing the following files:")
files = []
for r, d, f in os.walk(path):
    if ("01_Application" not in r) and ("02_RTE" not in r) and ("EcuM" not in r) and ("09_Templates" not in r):
        for file in f:
            if (".c" in file):
                str = os.path.abspath(os.path.join(r, file))
                print(str)        
                os.remove(str)